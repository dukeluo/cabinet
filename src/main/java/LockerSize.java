enum LockerSize {
    BIG(900),
    MEDIUM(700);

    private final int weight;

    LockerSize(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}
