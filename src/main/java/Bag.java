public class Bag {

    private final BagSize size;

    public Bag(BagSize size) {
        this.size = size;
    }

    public BagSize getSize() {
        return size;
    }
}
