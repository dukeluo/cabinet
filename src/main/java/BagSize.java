enum BagSize {
    HUGE(1000),
    BIG(800),
    MEDIUM(600),
    SMALL(400),
    MINI(200);

    private final int weight;

    BagSize(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}
