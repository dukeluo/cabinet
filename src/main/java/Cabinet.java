import java.util.HashMap;
import java.util.Map;

public class Cabinet {
    private Locker bigLocker;
    private Locker mediumLocker;

    public Cabinet(int bigCapacity, int mediumCapacity) {
        this.bigLocker = new Locker(LockerSize.BIG, bigCapacity);
        this.mediumLocker = new Locker(LockerSize.MEDIUM, mediumCapacity);
    }

    public Ticket save(Bag bag, LockerSize lockerSize) {
        if (bag == null) {
            throw new IllegalArgumentException("There is nothing");
        }
        if (lockerSize == null) {
            throw new IllegalArgumentException("Please give a specific locker size");
        }

        parameterCheck(bag, lockerSize);

        Ticket ticket = new Ticket();
        switch (lockerSize) {
            case BIG:
                bigLocker.throwIfInsufficient();
                bigLocker.put(ticket, bag);
                break;
            case MEDIUM:
                mediumLocker.throwIfInsufficient();
                mediumLocker.put(ticket, bag);
                break;
            default:
                break;
        }

        return ticket;
    }

    private void parameterCheck(Bag bag, LockerSize lockerSize) {
        if (lockerSize.getWeight() < bag.getSize().getWeight()) {
            throw new IllegalArgumentException("Overload");
        }
    }

    public Bag get(Ticket ticket) {
        if (ticket == null) {
            throw new IllegalArgumentException("There is no ticket");
        }

        boolean isBigContain = bigLocker.contain(ticket);
        boolean isMediumContain = mediumLocker.contain(ticket);

        if (!isBigContain && !isMediumContain) {
            throw new IllegalArgumentException("Wrong ticket");
        }

        return isBigContain ? bigLocker.get(ticket) : mediumLocker.get(ticket);
    }
}
