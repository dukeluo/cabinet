import java.util.HashMap;
import java.util.Map;

class Locker {
    private final int capacity;
    private final LockerSize lockerSize;

    private final Map<Ticket, Bag> lockers = new HashMap<Ticket, Bag>();

    public Locker(LockerSize lockerSize, int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Capacity is invalid");
        }

        this.lockerSize = lockerSize;
        this.capacity = capacity;
    }

    private boolean isInsufficient() {
        return lockers.size() >= capacity;
    }

    void throwIfInsufficient() {
        if (isInsufficient())
            throw new InsufficientLockersException("Insufficient empty lockers");
    }

    void put(Ticket ticket, Bag bag) {
        lockers.put(ticket, bag);
    }

    Bag get(Ticket ticket) {
        Bag bag = lockers.get(ticket);
        lockers.remove(ticket);
        return bag;
    }

    boolean contain(Ticket ticket) {
        return lockers.containsKey(ticket);
    }
}
