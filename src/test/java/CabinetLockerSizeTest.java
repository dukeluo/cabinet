import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CabinetLockerSizeTest {

    private static Object[][] createSmallerBagBiggerLockerArguments() {
        return new Object[][]{
                { LockerSize.BIG, BagSize.BIG },
                { LockerSize.BIG, BagSize.MEDIUM },
                { LockerSize.BIG, BagSize.SMALL },
                { LockerSize.BIG, BagSize.MINI },
                { LockerSize.MEDIUM, BagSize.MEDIUM },
                { LockerSize.MEDIUM, BagSize.SMALL },
                { LockerSize.MEDIUM, BagSize.MINI },
        };
    }

    private static Object[][] createBiggerBagSmallerLockerArguments() {
        return new Object[][]{
                { LockerSize.BIG, BagSize.HUGE },
                { LockerSize.MEDIUM, BagSize.HUGE },
                { LockerSize.MEDIUM, BagSize.BIG },
        };
    }

    @ParameterizedTest
    @MethodSource("createSmallerBagBiggerLockerArguments")
    void should_get_bag_given_valid_locker_size_when_bag_smaller_then_locker_ticket(LockerSize lockerSize, BagSize bagSize) {
        Cabinet cabinet = new Cabinet(10, 10);
        Bag bag = new Bag(bagSize);
        Ticket ticket = cabinet.save(bag, lockerSize);

        assertSame(bag, cabinet.get(ticket));
    }

    @ParameterizedTest
    @MethodSource("createBiggerBagSmallerLockerArguments")
    void should_throw_given_valid_locker_size_when_bag_bigger_than_locker_ticket(LockerSize lockerSize, BagSize bagSize) {
        Cabinet cabinet = new Cabinet(10, 10);
        Bag bag = new Bag(bagSize);

        assertThrows(IllegalArgumentException.class, () -> {
            cabinet.save(bag, lockerSize);
        });
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"MEDIUM", "SMALL", "MINI"})
    void should_throw_given_medium_full_cabinet_when_saving_valid_bag(BagSize bagSize) {
        Cabinet cabinet = new Cabinet(0, 1);
        Bag bag = new Bag(bagSize);
        cabinet.save(bag, LockerSize.MEDIUM);

        assertThrows(InsufficientLockersException.class, () -> {
            cabinet.save(bag, LockerSize.MEDIUM);
        });
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"BIG", "MEDIUM", "SMALL", "MINI"})
    void should_throw_given_big_full_cabinet_when_saving_valid_bag(BagSize bagSize) {
        Cabinet cabinet = new Cabinet(1, 0);
        Bag bag = new Bag(bagSize);
        cabinet.save(bag, LockerSize.BIG);

        assertThrows(InsufficientLockersException.class, () -> {
            cabinet.save(bag, LockerSize.BIG);
        });
    }
}
