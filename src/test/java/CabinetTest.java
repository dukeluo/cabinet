import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CabinetTest {
    @Test
    void should_get_a_ticket_when_saving_bag_to_the_cabinet() {
        Cabinet cabinet = new Cabinet(10, 0);
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = cabinet.save(bag, LockerSize.BIG);

        assertNotNull(ticket);
    }

    @Test
    void should_throw_when_saving_nothing_to_the_cabinet() {
        Cabinet cabinet = new Cabinet(10, 0);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> cabinet.save((Bag) null, LockerSize.BIG));
        assertEquals("There is nothing", exception.getMessage());
    }

    @Test
    void should_get_bag_when_using_corresponding_ticket() {
        Cabinet cabinet = new Cabinet(10, 0);
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = cabinet.save(bag, LockerSize.BIG);

        assertSame(bag, cabinet.get(ticket));
    }

    @Test
    void should_throw_when_get_bag_without_ticket() {
        Cabinet cabinet = new Cabinet(10, 0);
        Bag bag = new Bag(BagSize.BIG);
        cabinet.save(bag, LockerSize.BIG);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                cabinet.get(null));
        assertEquals("There is no ticket", exception.getMessage());
    }

    @Test
    void should_throw_when_get_bag_without_corresponding_ticket() {
        Cabinet cabinet = new Cabinet(10, 0);
        Cabinet anotherCabinet = new Cabinet(10, 0);
        Ticket anotherTicket = anotherCabinet.save(new Bag(BagSize.BIG), LockerSize.BIG);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                cabinet.get(anotherTicket));
        assertEquals("Wrong ticket", exception.getMessage());
    }

    @Test
    void should_throw_when_get_bag_with_used_ticket() {
        Cabinet cabinet = new Cabinet(10, 0);
        Ticket ticket = cabinet.save(new Bag(BagSize.BIG), LockerSize.BIG);
        cabinet.get(ticket);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                cabinet.get(ticket));
        assertEquals("Wrong ticket", exception.getMessage());
    }

    @Test
    void should_save_bag_when_cabinet_is_not_full() {
        Cabinet cabinet = new Cabinet(10, 0);
        assertNotNull(cabinet.save(new Bag(BagSize.BIG), LockerSize.BIG));
    }

    @Test
    void should_throw_when_size_is_invalid() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new Cabinet(-1, 0));
        assertEquals("Capacity is invalid", exception.getMessage());
    }

    @Test
    void should_throw_when_cabinet_is_full() {
        Cabinet cabinet = new Cabinet(1, 0);
        cabinet.save(new Bag(BagSize.BIG), LockerSize.BIG);

        InsufficientLockersException exception = assertThrows(InsufficientLockersException.class, () -> cabinet.save(new Bag(BagSize.BIG), LockerSize.BIG));
        assertEquals("Insufficient empty lockers", exception.getMessage());
    }

    @Test
    void should_throw_when_saving_bag_without_locker_size() {
        Cabinet cabinet = new Cabinet(10, 0);
        Bag bag = new Bag(BagSize.BIG);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            cabinet.save(bag, null);
        });
        assertEquals("Please give a specific locker size", exception.getMessage());
    }
}
